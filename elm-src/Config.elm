module Config
    exposing
        ( imageInputId
        , maxStage
        , nonPrimeImageNumberId
        )


maxStage : Int
maxStage =
    2


imageInputId : String
imageInputId =
    "file"


nonPrimeImageNumberId : String
nonPrimeImageNumberId =
    "nonPrimeImageNumberId"
