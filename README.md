# PrimeImage

Turn any image into an prime number.
See https://harrysarson.github.io/PrimeImage.

**Master**: [![Build Status](https://travis-ci.com/harrysarson/primeimage.svg?branch=master)](https://travis-ci.com/harrysarson/primeimage)
